'use strict'

const routesData = require("../src/seedersData/routesData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('routes', routesData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('routes', null, {})
  }
}
