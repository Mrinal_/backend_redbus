'use strict'

const seatsData = require("../src/seedersData/seatsData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('seats', seatsData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('seats', null, {})
  }
}