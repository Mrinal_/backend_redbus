'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("users",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            firstName: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            lastName: {
                type: Sequelize.STRING(100)
            },
            gender: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },
            dob: {
                type: Sequelize.DATEONLY,
                allowNull: false,
            },
            emailId: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            password: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            phoneNumber: {
                type: Sequelize.STRING(100),
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("users")
    }
}