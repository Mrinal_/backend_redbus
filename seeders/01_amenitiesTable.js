'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("amenities",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("amenities")
    }
}
