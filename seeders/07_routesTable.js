'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("routes",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            departureTime: {
                type: Sequelize.TIME(),
                allowNull: false,
            },
            arrivalTime: {
                type: Sequelize.TIME(),
                allowNull: false,
            },
            startDate: {
                type: Sequelize.DATEONLY(),
                allowNull: false,
            },
            endDate: {
                type: Sequelize.DATEONLY(),
                allowNull: false,
            },
            fare: {
                type: Sequelize.INTEGER(),
                allowNull: false,
            },
            bus_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "buses",
                    key: "id",
                },
            },
            sourceCity: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "cities",
                    key: "id",
                },
            },
            destinationCity: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "cities",
                    key: "id",
                },
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("routes")
    }
}