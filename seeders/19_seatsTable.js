'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("seats",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            seatNumber: {
                type: Sequelize.INTEGER(),
                allowNull: false,
            },
            isBooked: {
                type: Sequelize.BOOLEAN(),
            },
            route_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "routes",
                    key: "id"
                },
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("seats")
    }
}
