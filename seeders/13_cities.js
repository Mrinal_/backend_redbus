'use strict'

const citiesData = require("../src/seedersData/citiesData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('cities', citiesData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('cities', null, {})
  }
}