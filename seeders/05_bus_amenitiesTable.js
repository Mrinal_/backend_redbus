'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("bus_amenities",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            bus_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "buses",
                    key: "id",
                },
            },
            amenities_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "amenities",
                    key: "id",
                },
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("bus_amenities")
    }
}