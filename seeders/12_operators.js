'use strict'

const operatorsData = require("../src/seedersData/operatorsData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('operators', operatorsData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('operators', null, {})
  }
}
