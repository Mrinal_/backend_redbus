'use strict'

const busesData = require("../src/seedersData/busesData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('buses', busesData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('buses', null, {})
  }
}
