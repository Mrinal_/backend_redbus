'use strict'

const busStopsData = require("../src/seedersData/busStopsData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('busStops', busStopsData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('busStops', null, {})
  }
}
