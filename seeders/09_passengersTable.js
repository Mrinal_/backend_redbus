'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("passengers",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            name: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            age: {
                type: Sequelize.INTEGER(),
                allowNull: false,
            },
            seatNumber: {
                type: Sequelize.INTEGER(),
                allowNull: false,
            },
            trip_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "trips",
                    key: "id",
                },
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("passengers")
    }
}