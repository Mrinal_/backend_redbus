'use strict'

const Sequelize = require("sequelize")

module.exports = {
    up: async (queryInterface) => {
        await queryInterface.createTable("tripStops",{
            id: {
                type: Sequelize.INTEGER(),
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            trip_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "trips",
                    key: "id",
                },
            },
            busStop_id: {
                type: Sequelize.INTEGER(),
                references: {
                    model: "busStops",
                    key: "id",
                },
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type:Sequelize.DATE,
            },
        })
    },

    down: async (queryInterface) => {
        await queryInterface.dropTable("tripStops")
    }
}