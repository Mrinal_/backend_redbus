'use strict'

const bus_amenitiesData = require("../src/seedersData/bus_amenitiesData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('bus_amenities', bus_amenitiesData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('bus_amenities', null, {})
  }
}