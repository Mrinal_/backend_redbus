'use strict'

const amenitiesData = require("../src/seedersData/amenitiesData")

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.bulkInsert('amenities', amenitiesData)
  },

  down: async (queryInterface) => {
    return queryInterface.bulkDelete('amenities', null, {})
  }
}
