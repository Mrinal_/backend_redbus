const Joi = require("joi")
const phoneNumberJoi = Joi.extend(require('joi-phone-number'));

const signupAuthSchema = Joi.object({
    firstName : Joi.string()
                .label("First name")
                .required()
                .messages({
                    'string.base': `First name should be a type of 'text'`,
                    'any.required': `First name is required`
                }),

    lastName: Joi.string()
              .allow("")
              .label("Last name"),

    gender: Joi.string()
            .required()
            .label("Gender")
            .messages({
                'string.required': `Gender is required`
            }),

    dob: Joi.date()
         .required()
         .label("Date of Birth")
         .messages({
            'any.required': `Date of Birth is required`
         }),

    email:  Joi.string().email({
                tlds: false
            })
            .required()
            .label("Email")
            .messages({
                'any.required': `Email is required`
            }),

    password: Joi.string()
              .min(4)
              .required()
              .label("Password")
              .messages({
                'string.min': `Password should have a minimum length of {{#limit}}`,
                'any.required': `Password is required`
              }),
})

const loginAuthSchema = Joi.object({
    email:  Joi.string().email({
                tlds: false
            })
            .required()
            .label("Email")
            .messages({
                'any.required': `Email is required`
            }),

    password: Joi.string()
              .required()
              .label("Password")
              .messages({
                'any.required': `Password is required`
              }),
})

const searchCityAuthSchema = Joi.object({
    cityName: Joi.string()
              .label("City name")
              .required()
              .messages({
                'any.required': `City name is required`
              }),
})

const searchBusAuthSchema = Joi.object({
    sourceId: Joi.number()
              .required()
              .messages({
                'any.required': `Source city is required`
              }),

    destinationId: Joi.number()
                   .required()
                   .messages({
                     'any.required': `Destination city is required`
                   }),  

    date: Joi.date()
          .required()
          .label("Journey date")
          .messages({
            'any.required': `Journey date is required`
          }),
})

const updateUserAuthSchema = Joi.object({
  userId: Joi.number()
          .required(),

  firstName: Joi.string()
             .allow("")
             .label("First name")
             .messages({
                'string.base': `First name should be a type of 'text'`,
              }),

  lastName: Joi.string()
            .allow("")
            .label("Last name")
            .messages({
              'string.base': `First name should be a type of 'text'`,
            }),

  phoneNumber: phoneNumberJoi.string()
               .phoneNumber({ defaultCountry: "IN", strict: true})
               .allow("")
               .label("Phone number"),
})

module.exports = {
  signupAuthSchema,
  loginAuthSchema,
  searchCityAuthSchema,
  searchBusAuthSchema,
  updateUserAuthSchema,
}