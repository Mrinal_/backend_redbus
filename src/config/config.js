module.exports = {
    port     : process.env.PORT || 3000,
    host     : process.env.host,
    database : process.env.database,
    user     : process.env.user,
    password : process.env.password,
    dialect  : "mysql",
    pool     : {
        min : 0,
        max : 5,
        acquire: 30000,
        idle: 10000
    },
    secret_key: process.env.secret_key,
}