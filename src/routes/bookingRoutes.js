const express = require("express")
const router = express.Router()
const bookingController = require("../controller/bookingController.js")

router.put("/bookSeats", bookingController.seatsBooking)

router.post("/allBookings", bookingController.allBookings)

router.put("/cancelBooking", bookingController.cancelBooking)

module.exports = router