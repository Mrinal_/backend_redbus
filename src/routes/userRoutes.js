const express = require("express")
const router = express.Router()
const User = require("../models/User")
const userController = require("../controller/userController.js")
const jwt = require("jsonwebtoken")
const { secret_key } = require("../config/config")

router.get("/getUserByToken", (request, response, next) => {
    jwt.verify(request.token, secret_key, async(error, user)=>{
        if(error){
            return response.status(403).json({
                message: "Please log in again"
            })
        }
        else{
            const userId = user.user.id

            const userData = await User.findAll({
                where: {
                    id: userId,
                }
            })

            const { password, ...userDetails } = userData[0].dataValues

            return response.json({
                user: userDetails
            })
        }
    })
})

router.put("/updateUser", userController.updateUser)

module.exports = router