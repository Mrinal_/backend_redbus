const express = require("express")
const router = express.Router()
const searchController = require("../controller/searchController.js")

router.post("/searchCity", searchController.cityName)

router.post("/searchBus", searchController.searchBus)

router.get("/allCities", searchController.searchAllCities)

module.exports = router