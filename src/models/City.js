const Sequelize = require("sequelize")
const { connection } = require("./Connection")

const City = connection.define("cities", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
})

module.exports = City