const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const Trip = require("./Trip")

const Ticket = connection.define("tickets", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    fare: {
        type: Sequelize.INTEGER(),
        allowNull: false,
    }
})

Ticket.belongsTo(Trip, {foreignKey: "trip_id", targetKey: "id"})

module.exports = Ticket