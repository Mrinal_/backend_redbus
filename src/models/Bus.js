const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const Operator = require("./Operator")

const Bus = connection.define("buses", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
})

Operator.hasMany(Bus, {foreignKey: "operator_id"})

module.exports = Bus