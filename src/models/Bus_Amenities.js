const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const Bus = require("./Bus")
const Amenities = require("./Amenities")

const Bus_Amenities = connection.define("bus_amenities", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
})

Bus.belongsToMany(Amenities, {through: "bus_amenities", foreignKey: "bus_id", otherKey: "amenities_id"})

module.exports = Bus_Amenities