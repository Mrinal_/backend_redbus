const Sequelize = require("sequelize")
const { connection } = require("./Connection")

const Operator = connection.define("operators", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
})

module.exports = Operator