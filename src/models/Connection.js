const Sequelize = require("sequelize")
const config = require("../config/config")
const Umzug = require("umzug")
const path = require("path")

const connection = new Sequelize(
    config.database,
    config.user,
    config.password,
    {
        host : config.host,
        dialect : config.dialect,

        pool : {
            max     : config.pool.max,
            min     : config.pool.min,
            acquire : config.pool.acquire,
            idle    : config.pool.idle,
        }
    }
)

const umzug = new Umzug({
    migrations: {
        path: path.join(__dirname, '../../seeders'),
        params: [
            connection.getQueryInterface()
        ],
        storage: 'sequelize',
        storageOptions: {
            sequelize: connection,
        }
    }
})

module.exports = {
    connection,
    umzug,
}