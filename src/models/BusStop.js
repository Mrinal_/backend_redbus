const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const City = require("./City")

const BusStop = connection.define("busStops", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
})

module.exports = BusStop