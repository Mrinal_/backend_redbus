const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const Trip = require("./Trip")
const BusStop = require("./BusStop")

const TripStops = connection.define("tripStops", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
})

TripStops.belongsTo(Trip, {foreignKey: "trip_id", targetKey: "id"})
TripStops.belongsTo(BusStop, {foreignKey: "busStop_id", targetKey: "id"})

module.exports = TripStops