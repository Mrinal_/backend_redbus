const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const City = require("./City")
const Bus = require("./Bus")
const User = require("./User")

const Trip = connection.define("trips", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    departureTime: {
        type: Sequelize.TIME(),
        allowNull: false,
    },
    arrivalTime: {
        type: Sequelize.TIME(),
        allowNull: false,
    },
    startDate: {
        type: Sequelize.DATEONLY(),
        allowNull: false,
    },
    endDate: {
        type: Sequelize.DATEONLY(),
        allowNull: false,
    }
})

Trip.belongsTo(User, {foreignKey: "user_id", targetKey: "id"})

module.exports = Trip