const Sequelize = require("sequelize")
const { connection } = require("./Connection")

const Seats = connection.define("seats",{
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    seatNumber: {
        type: Sequelize.INTEGER(),
        allowNull: false,
    },
    isBooked: {
        type: Sequelize.BOOLEAN(),
    },
})

module.exports = Seats