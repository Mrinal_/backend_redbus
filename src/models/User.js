const Sequelize = require("sequelize")
const { connection } = require("./Connection")

const User = connection.define("users", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    firstName: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
    lastName: {
        type: Sequelize.STRING(100)
    },
    gender: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },
    dob: {
        type: Sequelize.DATEONLY,
        allowNull: false,
    },
    emailId: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
    phoneNumber: {
        type: Sequelize.STRING(100),
    },
})

module.exports = User