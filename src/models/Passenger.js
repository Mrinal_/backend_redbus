const Sequelize = require("sequelize")
const { connection } = require("./Connection")
const Trip = require("./Trip")

const Passenger = connection.define("passengers", {
    id: {
        type: Sequelize.INTEGER(),
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING(100),
        allowNull: false,
    },
    age: {
        type: Sequelize.INTEGER(),
        allowNull: false,
    },
    seatNumber: {
        type: Sequelize.INTEGER(),
        allowNull: false,
    },
})

Passenger.belongsTo(Trip, {foreignKey: "trip_id", targetKey: "id"})

module.exports = Passenger