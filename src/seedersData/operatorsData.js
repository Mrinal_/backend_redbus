const operatorsData = [
    {
        id: 1,
        name: 'SRS Travels',
    },
    {
        id: 2,
        name: 'Rajdhani Express',
    },
    {
        id: 3,
        name: 'Royal Travels',
    },
    {
        id: 4,
        name: 'Shatabdi Travels',
    },
    {
        id: 5,
        name: 'KPN Travels',
    },
    {
        id: 6,
        name: 'Humsafar Travels',
    },
    {
        id: 7,
        name: 'Universal Travels',
    },
    {
        id: 8,
        name: 'Eagle Travels',
    },
    {
        id: 9,
        name: 'VRL Travels',
    },
    {
        id: 10,
        name: 'MR Travels',
    },
]

module.exports = operatorsData