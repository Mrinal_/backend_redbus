const citiesData = [
    {
        id: 1,
        name: 'Patna',
    },
    {
        id: 2,
        name: 'Banaras',
    },
    {
        id: 3,
        name: 'Kolkata',
    },
    {
        id: 4,
        name: 'Hyderabad',
    },
    {
        id: 5,
        name: 'Chennai',
    },
    {
        id: 6,
        name: 'Bangalore',
    },
    {
        id: 7,
        name: 'Pune',
    },
    {
        id: 8,
        name: 'Mumbai',
    },
    {
        id: 9,
        name: 'New Delhi',
    },
    {
        id: 10,
        name: 'Goa',
    },
]

module.exports = citiesData