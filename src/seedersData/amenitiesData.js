const amenitiesData = [
    {
        id: 1,
        name: 'liveTracking',
    },
    {
        id: 2,
        name: 'reschedulable',
    },
    {
        id: 3,
        name: 'wifi',
    },
    {
        id: 4,
        name: 'waterBottle',
    },
    {
        id: 5,
        name: 'chargingPoint',
    },
    {
        id: 6,
        name: 'ac',
    },
]

module.exports = amenitiesData