const busStopsData = [
    {
        id: 1,
        name: "G Maidan",
        city_id: 1,
    },
    {
        id: 2,
        name: "Meethapur Bus Stand",
        city_id: 1,
    },
    {
        id: 3,
        name: "Varanasi Bus Stand",
        city_id: 2,
    },
    {
        id: 4,
        name: "Mohansarai Petrol Pump",
        city_id: 2,
    },
    {
        id: 5,
        name: "Esplanade",
        city_id: 3,
    },
    {
        id: 6,
        name: "Howrah",
        city_id: 3,
    },
    {
        id: 7,
        name: "Secunderabad",
        city_id: 4,
    },
    {
        id: 8,
        name: "Begumpet",
        city_id: 4,
    },
    {
        id: 9,
        name: "Lakdikapul",
        city_id: 4,
    },
    {
        id: 10,
        name: "Red Hills",
        city_id: 5,
    },
    {
        id: 11,
        name: "Anna Nagar",
        city_id: 5,
    },
    {
        id: 12,
        name: "Ashok Pillar",
        city_id: 5,
    },
    {
        id: 13,
        name: "Electronic City",
        city_id: 6,
    },
    {
        id: 14,
        name: "Anand Rao Circle",
        city_id: 6,
    },
    {
        id: 15,
        name: "BTM Layout",
        city_id: 6,
    },
    {
        id: 16,
        name: "Akurdi",
        city_id: 7,
    },
    {
        id: 17,
        name: "Katraj",
        city_id: 7,
    },
    {
        id: 18,
        name: "Chandan Nagar",
        city_id: 7,
    },
    {
        id: 19,
        name: "Andheri",
        city_id: 8,
    },
    {
        id: 20,
        name: "Borivali",
        city_id: 8,
    },
    {
        id: 21,
        name: "Ghatkopar",
        city_id: 8,
    },
    {
        id: 22,
        name: "Adarsh Nagar",
        city_id: 9,
    },
    {
        id: 23,
        name: "Dwarka",
        city_id: 9,
    },
    {
        id: 24,
        name: "India Gate",
        city_id: 9,
    },
    {
        id: 25,
        name: "Kankavli",
        city_id: 10,
    },
    {
        id: 26,
        name: "Panjim",
        city_id: 10,
    },
]

module.exports = busStopsData