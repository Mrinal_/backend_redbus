const busesData = [
    {
        id: 1,
        operator_id: 1,
    },
    {
        id: 2,
        operator_id: 1,
    },
    {
        id: 3,
        operator_id: 2,
    },
    {
        id: 4,
        operator_id: 2,
    },
    {
        id: 5,
        operator_id: 3,
    },
    {
        id: 6,
        operator_id: 4,
    },
    {
        id: 7,
        operator_id: 4,
    },
    {
        id: 8,
        operator_id: 4,
    },
    {
        id: 9,
        operator_id: 5,
    },
    {
        id: 10,
        operator_id: 5,
    },
    {
        id: 11,
        operator_id: 6,
    },
    {
        id: 12,
        operator_id: 7,
    },
    {
        id: 13,
        operator_id: 7,
    },
    {
        id: 14,
        operator_id: 7,
    },
    {
        id: 15,
        operator_id: 8,
    },
    {
        id: 16,
        operator_id: 8,
    },
    {
        id: 17,
        operator_id: 9,
    },
    {
        id: 18,
        operator_id: 10,
    },
    {
        id: 19,
        operator_id: 10,
    },
    {
        id: 20,
        operator_id: 10,
    },
]

module.exports = busesData