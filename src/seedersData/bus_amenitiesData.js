const bus_amenitiesData = [
    {
        id: 1,
        bus_id: 1,
        amenities_id: 1,
    },
    {
        id: 2,
        bus_id: 1,
        amenities_id: 2,
    },
    {
        id: 3,
        bus_id: 1,
        amenities_id: 3,
    },
    {
        id: 4,
        bus_id: 1,
        amenities_id: 4,
    },
    {
        id: 5,
        bus_id: 1,
        amenities_id: 6,
    },
    {
        id: 6,
        bus_id: 2,
        amenities_id: 2,
    },
    {
        id: 7,
        bus_id: 2,
        amenities_id: 4,
    },
    {
        id: 8,
        bus_id: 2,
        amenities_id: 5,
    },
    {
        id: 9,
        bus_id: 3,
        amenities_id: 1,
    },
    {
        id: 10,
        bus_id: 3,
        amenities_id: 4,
    },
    {
        id: 11,
        bus_id: 3,
        amenities_id: 5,
    },
    {
        id: 12,
        bus_id: 3,
        amenities_id: 6,
    },
    {
        id: 13,
        bus_id: 4,
        amenities_id: 2,
    },
    {
        id: 14,
        bus_id: 4,
        amenities_id: 4,
    },
    {
        id: 15,
        bus_id: 4,
        amenities_id: 5,
    },
    {
        id: 16,
        bus_id: 5,
        amenities_id: 1,
    },
    {
        id: 17,
        bus_id: 5,
        amenities_id: 2,
    },
    {
        id: 18,
        bus_id: 5,
        amenities_id: 3,
    },
    {
        id: 19,
        bus_id: 5,
        amenities_id: 4,
    },
    {
        id: 20,
        bus_id: 5,
        amenities_id: 6,
    },
    {
        id: 21,
        bus_id: 6,
        amenities_id: 2,
    },
    {
        id: 22,
        bus_id: 6,
        amenities_id: 4,
    },
    {
        id: 23,
        bus_id: 6,
        amenities_id: 5,
    },
    {
        id: 24,
        bus_id: 7,
        amenities_id: 1,
    },
    {
        id: 25,
        bus_id: 7,
        amenities_id: 4,
    },
    {
        id: 26,
        bus_id: 7,
        amenities_id: 5,
    },
    {
        id: 27,
        bus_id: 7,
        amenities_id: 6,
    },
    {
        id: 28,
        bus_id: 8,
        amenities_id: 2,
    },
    {
        id: 29,
        bus_id: 8,
        amenities_id: 4,
    },
    {
        id: 30,
        bus_id: 8,
        amenities_id: 5,
    },
    {
        id: 31,
        bus_id: 9,
        amenities_id: 1,
    },
    {
        id: 32,
        bus_id: 9,
        amenities_id: 2,
    },
    {
        id: 33,
        bus_id: 9,
        amenities_id: 3,
    },
    {
        id: 34,
        bus_id: 9,
        amenities_id: 4,
    },
    {
        id: 35,
        bus_id: 9,
        amenities_id: 6,
    },
    {
        id: 36,
        bus_id: 10,
        amenities_id: 2,
    },
    {
        id: 37,
        bus_id: 10,
        amenities_id: 4,
    },
    {
        id: 38,
        bus_id: 10,
        amenities_id: 5,
    },
    {
        id: 39,
        bus_id: 11,
        amenities_id: 1,
    },
    {
        id: 40,
        bus_id: 11,
        amenities_id: 4,
    },
    {
        id: 41,
        bus_id: 11,
        amenities_id: 5,
    },
    {
        id: 42,
        bus_id: 11,
        amenities_id: 6,
    },
    {
        id: 43,
        bus_id: 12,
        amenities_id: 2,
    },
    {
        id: 44,
        bus_id: 12,
        amenities_id: 4,
    },
    {
        id: 45,
        bus_id: 12,
        amenities_id: 5,
    },
    {
        id: 46,
        bus_id: 13,
        amenities_id: 1,
    },
    {
        id: 47,
        bus_id: 13,
        amenities_id: 2,
    },
    {
        id: 48,
        bus_id: 13,
        amenities_id: 3,
    },
    {
        id: 49,
        bus_id: 13,
        amenities_id: 4,
    },
    {
        id: 50,
        bus_id: 13,
        amenities_id: 6,
    },
    {
        id: 51,
        bus_id: 14,
        amenities_id: 2,
    },
    {
        id: 52,
        bus_id: 14,
        amenities_id: 4,
    },
    {
        id: 53,
        bus_id: 14,
        amenities_id: 5,
    },
    {
        id: 54,
        bus_id: 15,
        amenities_id: 1,
    },
    {
        id: 55,
        bus_id: 15,
        amenities_id: 4,
    },
    {
        id: 56,
        bus_id: 15,
        amenities_id: 5,
    },
    {
        id: 57,
        bus_id: 15,
        amenities_id: 6,
    },
    {
        id: 58,
        bus_id: 16,
        amenities_id: 2,
    },
    {
        id: 59,
        bus_id: 16,
        amenities_id: 4,
    },
    {
        id: 60,
        bus_id: 16,
        amenities_id: 5,
    },
    {
        id: 61,
        bus_id: 17,
        amenities_id: 1,
    },
    {
        id: 62,
        bus_id: 17,
        amenities_id: 2,
    },
    {
        id: 63,
        bus_id: 17,
        amenities_id: 3,
    },
    {
        id: 64,
        bus_id: 17,
        amenities_id: 4,
    },
    {
        id: 65,
        bus_id: 17,
        amenities_id: 6,
    },
    {
        id: 66,
        bus_id: 18,
        amenities_id: 2,
    },
    {
        id: 67,
        bus_id: 18,
        amenities_id: 4,
    },
    {
        id: 68,
        bus_id: 18,
        amenities_id: 5,
    },
    {
        id: 69,
        bus_id: 19,
        amenities_id: 1,
    },
    {
        id: 70,
        bus_id: 19,
        amenities_id: 4,
    },
    {
        id: 71,
        bus_id: 19,
        amenities_id: 5,
    },
    {
        id: 72,
        bus_id: 19,
        amenities_id: 6,
    },
    {
        id: 73,
        bus_id: 20,
        amenities_id: 2,
    },
    {
        id: 74,
        bus_id: 20,
        amenities_id: 4,
    },
    {
        id: 75,
        bus_id: 20,
        amenities_id: 5,
    },
]

module.exports = bus_amenitiesData