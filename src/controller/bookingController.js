const Seats = require("../models/Seats")
const Route = require("../models/Route")
const Trip = require("../models/Trip")
const Ticket = require("../models/Ticket")
const User = require("../models/User")
const City = require("../models/City")
const Bus = require("../models/Bus")
const Operator = require("../models/Operator")
const Sequelize = require("sequelize")
const Passenger = require("../models/Passenger")
const { response } = require("express")
const api_key = '5c0bb7f051765fb622e2095966684bb9-b6190e87-af20f97c'
const domain = 'sandbox5b329002b8c64f41885fd354d07e5637.mailgun.org'
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain})
const Op = Sequelize.Op

const seatsBooking = async(request, response, next) => {
    try{
        const { routeId, userId, fare } = request.body

        const seats = JSON.parse(request.body.seats)

        const passengers = JSON.parse(request.body.passengers)

        const checkSeats = seats.map((seat)=>{
            return Seats.findAll({
                attributes: ["seatNumber","isBooked"],
                where: {
                    [Op.and]: [
                        { route_id: routeId },
                        { seatNumber: seat }
                    ]
                },
            })
        })

        const seatsData= await Promise.all(checkSeats)

        let seatNumbers = []

        seatsData.map((seats)=>{
            seats.map((seat)=>{
                if(seat.isBooked == false){
                    Seats.update(
                        {
                            isBooked: true,
                        },
                        {
                            where: {
                                [Op.and]: [
                                    { route_id: routeId },
                                    { seatNumber: seat.seatNumber }
                                ]
                            },
                        },
                    )
                    .catch((error)=>{
                        return next(error)
                    })
                }
                else{
                    seatNumbers.push(seat.seatNumber)
                }
            })
        })

        if(seatNumbers.length == 0){

            const routeData = await Route.findAll({
                where: {
                    id: routeId,
                },
            })

            const routeDetails = routeData.map((route)=>{
                return {
                    route_id: route.id,
                    departureTime: route.departureTime,
                    arrivalTime: route.arrivalTime,
                    startDate: route.startDate,
                    endDate: route.endDate,
                    bus_id: route.bus_id,
                    sourceCity: route.sourceCity,
                    destinationCity: route.destinationCity,
                }
            })

            const tripData = {...routeDetails[0], user_id: userId}

            const tripDetails = await Trip.create(tripData)

            const ticketDetails = await Ticket.create({
                fare: fare,
                trip_id: tripDetails.id,
            })

            let passengersArray = passengers.map((passenger,index)=>{
                return Passenger.create({...passenger, seatNumber: seats[index], trip_id: tripDetails.id})
            })

            let seat_numbers_booked = ""

            const passengersDetails = await Promise.all(passengersArray)

            passengersDetails.forEach((passenger)=>{
                seat_numbers_booked = seat_numbers_booked.concat(` ${passenger.seatNumber} and`)
            })

            const sourceCity = await City.findByPk(routeDetails[0].sourceCity)

            const sourceCityName = sourceCity.dataValues.name

            const destinationCity = await City.findByPk(routeDetails[0].destinationCity)

            const destinationCityName = destinationCity.dataValues.name

            const userDetails = await User.findAll({
                attributes: ["emailId"],
                where: {
                    id: userId
                }
            })

            const data = {
                from: 'My Bus <mrinal.singh@mountblue.tech>',
                to: `${userDetails[0].dataValues.emailId}`,
                subject: 'Bus Ticket',
                html: ` 
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>My Bus</title>
                        <style>
                            body {
                                margin: 0px;
                                background-color: #045676;
                            }
                            h1 {
                                text-align: center;
                                color: #ffffff;
                            }
                            .wrapper {
                                padding: 20px;
                                margin: 20px auto;
                                max-width: 500px;
                                width: 100%;
                                background-color: #ffffff;
                                font-size: 22px;
                            }
                        </style>
                    </head>
                    <body>
                    <div>
                        <h1>My Bus</h1>
                        <div class="wrapper">
                            <h3>Booking Successful</h3>
                            <strong>
                                Your ticket on ${routeDetails[0].startDate} with us has been successfully booked.<br>
                                ${sourceCityName} to ${destinationCityName}<br>
                                Your seats are${seat_numbers_booked.slice(0, -4)}.<br>
                                Ticket id: ${ticketDetails.id}<br>
                                Have a nice journey. 
                            </strong>
                        </div>
                    </div>
                    
                    </body>
                    </html>`,
            }
               
            mailgun.messages().send(data, function (error, body) {
                if(error){
                    return next(error)
                }
            })

            return response.status(200).json({
                message: "Booking successful"
            })
        }
        else{

            let bookedSeatsNumbers = ""

            seatNumbers.map((seat)=>{
                bookedSeatsNumbers = bookedSeatsNumbers.concat(` ${seat},`)
            })

            return response.status(400).json({
                message: `Seat number${bookedSeatsNumbers.slice(0, -1)} are already booked. Please select another seats`
            })
        }
            
    }
    catch(error){
        next(error)
    }
}

const allBookings = async(request, response, next) => {
    try{
        const { userId } = request.body

        const allBookingDetails = await Trip.findAll({
            where: {
                user_id: userId,
            },
            include: [
                {
                    model: Bus, 
                    required: true,
                    include: {
                            model: Operator, 
                            required: true, 
                        },
                },
                {
                    model: City,
                    required: true,
                    as: "sourceCityName",
                },
                {
                    model: City,
                    required: true,
                    as: "destinationCityName",
                },
                {
                    model: Passenger,
                    required: true,
                },
                {
                    model: Ticket,
                    required: true,
                }
            ]
        })

        let nowDate = new Date(); 
        let month = nowDate.getMonth()+1
        let day = nowDate.getDate()
        let currentDate = nowDate.getFullYear()+'-'+(month < 10 ? '0'+ month : month)+'-'+(day < 10 ? '0'+ day : day); 

        let upcomingTrips = []
        let completedTrips = []

        allBookingDetails.forEach((booking)=>{

            if(booking.startDate >= currentDate){
                upcomingTrips.push({
                    departureTime: booking.departureTime,
                    arrivalTime: booking.arrivalTime,
                    startDate: booking.startDate,
                    endDate: booking.endDate,
                    tripId: booking.id,
                    routeId: booking.route_id,
                    operator: booking.bus.operator.name,
                    fare: booking.ticket.fare,
                    ticketId: booking.ticket.id,
                    sourceCity: booking.sourceCityName.name,
                    destinationCity: booking.destinationCityName.name,
                    passengers:  booking.passengers.reduce((accumulator, passenger)=>{
                        accumulator.push({
                            name: passenger.name,
                            age: passenger.age,
                            seatNumber: passenger.seatNumber,
                        })
                        return accumulator
                    },[]),
                })
            }
            else{
                completedTrips.push({
                    departureTime: booking.departureTime,
                    arrivalTime: booking.arrivalTime,
                    startDate: booking.startDate,
                    endDate: booking.endDate,
                    tripId: booking.id,
                    routeId: booking.route_id,
                    operator: booking.bus.operator.name,
                    fare: booking.ticket.fare,
                    ticketId: booking.ticket.id,
                    sourceCity: booking.sourceCityName.name,
                    destinationCity: booking.destinationCityName.name,
                    passengers:  booking.passengers.reduce((accumulator, passenger)=>{
                        accumulator.push({
                            name: passenger.name,
                            age: passenger.age,
                            seatNumber: passenger.seatNumber,
                        })
                        return accumulator
                    },[]),
                })
            }
        })

        return response.status(200).json({
            upcomingTrips,
            completedTrips,
        })
    }
    catch(error){
        next(error)
    }   
}

const cancelBooking = async(request, response, next) => {
    try{
        const { tripId, userId, routeId } = request.body

        const passengersData = await Passenger.findAll({
            where: {
                trip_id: tripId,
            },
        })

        const routeData = await Route.findAll({
            where: {
                id: routeId,
            },
        })

        const routeDetails = routeData.map((route)=>{
            return {
                route_id: route.id,
                departureTime: route.departureTime,
                arrivalTime: route.arrivalTime,
                startDate: route.startDate,
                endDate: route.endDate,
                bus_id: route.bus_id,
                sourceCity: route.sourceCity,
                destinationCity: route.destinationCity,
            }
        })

        let seat_numbers_booked = ""

        passengersData.forEach((passenger)=>{
            seat_numbers_booked = seat_numbers_booked.concat(` ${passenger.seatNumber} and`)
        })

        const sourceCity = await City.findByPk(routeDetails[0].sourceCity)

        const sourceCityName = sourceCity.dataValues.name

        const destinationCity = await City.findByPk(routeDetails[0].destinationCity)

        const destinationCityName = destinationCity.dataValues.name

        const userDetails = await User.findAll({
            attributes: ["emailId"],
            where: {
                id: userId
            }
        })

        const data = {
            from: 'My Bus <mrinal.singh@mountblue.tech>',
            to: `${userDetails[0].dataValues.emailId}`,
            subject: 'Canceled Tickets',
            html: ` <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>My Bus</title>
                        <style>
                            body {
                                margin: 0px;
                                background-color: #045676;
                            }
                            h1 {
                                text-align: center;
                                color: #ffffff;
                            }
                            .wrapper {
                                padding: 20px;
                                margin: 20px auto;
                                max-width: 500px;
                                width: 100%;
                                background-color: #ffffff;
                                font-size: 22px;
                            }
                        </style>
                    </head>
                    <body>
                    <div>
                        <h1>My Bus</h1>
                        <div class="wrapper">
                            <strong>
                                Your trip on ${routeDetails[0].startDate} has cancelled.<br>
                                ${sourceCityName} to ${destinationCityName}<br>
                                Your seats are${seat_numbers_booked.slice(0, -4)}.<br>
                            </strong>
                        </div>
                    </div>
                    </body>
                    </html>`,
        }
           
        mailgun.messages().send(data, function (error, body) {
            if(error){
                return next(error)
            }
        })

        const seatsData = passengersData.map((passenger)=>{
            return Seats.update(
                {
                    isBooked: false,
                },
                {
                    where: {
                        [Op.and]: [
                            { route_id: routeId },
                            { seatNumber: passenger.seatNumber }
                        ]
                    },
                },
            )
            
        })

        const deletedSeats = await Promise.all(seatsData)

        const deletedTicket = await Ticket.destroy({
            where: {
                trip_id: tripId,
            }
        })

        const deletedPassengers = await Passenger.destroy({
            where: {
                trip_id: tripId,
            }
        })

        const deletedTrip = await Trip.destroy({
            where: {
                [Op.and]: [
                    { id: tripId },
                    { user_id: userId }
                ]
            }
        })

        return response.status(200).json({
            message: "Trip cancelled"
        })

    }
    catch(error){
        next(error)
    }
}
module.exports = {
    seatsBooking,
    allBookings,
    cancelBooking,
}