const City = require("../models/City")
const Route = require("../models/Route")
const Bus = require("../models/Bus")
const Operator = require("../models/Operator")
const Sequelize = require("sequelize")
const Amenities = require("../models/Amenities")
const Seats = require("../models/Seats")
const BusStop = require("../models/BusStop")
const { searchCityAuthSchema, searchBusAuthSchema } = require("../schemas/authSchemas")
const Op = Sequelize.Op

const cityName = async(request, response, next) => {
    try{
        const { cityName } = request.body

        const validationResult = await searchCityAuthSchema.validate(request.body)

        if(validationResult.error == null){

            const cityData = await City.findAll({
                limit: 5,
                attributes: ['id', 'name'],
                where: {
                    name: {
                        [Op.like]: '%' + cityName + '%'
                    },
                },
                order: [
                    ['name', 'ASC']
                ],
            })
            
            return response.status(200).json({
                cityData
            })
        }
        else{
            return response.status(400).json(validationResult.error.message)
        }
    }
    catch(error){
        next(error)
    }
}

const searchBus = async(request, response, next) => {
    try{
        const { sourceId, destinationId, date } = request.body

        const validationResult = await searchBusAuthSchema.validate(request.body)

        if(validationResult.error == null){

            const routeData = await Route.findAll({
                where: {
                    sourceCity: sourceId,
                    destinationCity: destinationId,
                    startDate: date,
                },
                include: [
                    {
                        model: Bus, 
                        required: true,
                        include: [
                            {
                                model: Operator, 
                                required: true, 
                            },
                            {
                                model: Amenities, 
                                required: true,
                            },
                        ]
                    },
                    {
                        model: Seats,
                        required: true,
                    },
                ]  
            })

            const boardingPoints = await BusStop.findAll({
                attributes: ["id", "name" ],
                where: {
                    city_id: sourceId,
                }
            })

            const droppingPoints = await BusStop.findAll({
                attributes: ["id", "name" ],
                where: {
                    city_id: destinationId,
                }
            })

            const allAmenities = await Amenities.findAll()

            const allAmenitiesArray = allAmenities.map((amenity)=>{
                return amenity.name
            })
            
            const routeDetails = routeData.map((route)=>{

                let availableSeats = 0
                let bookedSeats = 0

                const seats = route.seats.map((seat)=>{
                    if(seat.isBooked == true){
                        bookedSeats += 1
                    }
                    else{
                        availableSeats += 1
                    }
                    return{
                        seatNo: seat.seatNumber,
                        isBooked: seat.isBooked,
                    }
                })

                const availableAmenitiesArray = route.bus.amenities.map((amenity)=>{
                    return amenity.name
                })

                return {
                    id: route.id,
                    departureTime: route.departureTime,
                    arrivalTime: route.arrivalTime,
                    startDate: route.startDate,
                    endDate: route.endDate,
                    fare: route.fare,
                    busId: route.bus.id,
                    operatorId: route.bus.operator.id,
                    operatorName: route.bus.operator.name,
                    totalAvailableSeats: availableSeats,
                    totalBookedSeats: bookedSeats,
                    seats: seats,
                    boardingPoints,
                    droppingPoints,
                    amenities:  allAmenitiesArray.reduce((accumulator, amenity)=>{
                        if(availableAmenitiesArray.includes(amenity) == true){
                            accumulator[amenity] = true
                        }
                        else{
                            accumulator[amenity] = false
                        }
                        return accumulator
                    },{}),
                }
            })

            response.status(200).json({
                routeDetails
            })
        }
        else{
            return response.status(400).json(validationResult.error.message)
        }  
    }
    catch(error){
        next(error)
    }   
}

const searchAllCities = async(request, response, next) => {
    try{
        const citiesData = await City.findAll({
            attributes: ["id", "name"]
        })
        
        return response.status(200).json(
            citiesData
        )
    }
    catch(error){
        next(error)
    }
}

module.exports = {
    cityName,
    searchBus,
    searchAllCities,
}