const User = require("../models/User")
const config = require("../config/config")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const { signupAuthSchema, loginAuthSchema } = require("../schemas/authSchemas")

//Register new user
const signup = async(request, response, next)=>{
    try{
        const { firstName, lastName, gender, dob, email, password } = request.body

        const validationResult = await signupAuthSchema.validate(request.body)

        if(validationResult.error == null){

            const userData = await User.findAll({
                where: { 
                    emailId: email,
                }
            })
    
            if(userData.length > 0){
                return response.json({
                    message: "Email is already registered"
                })
            }
    
            let hashedPassword = await bcrypt.hash(password, 10)
    
            let user = {
                firstName,
                lastName,
                gender,
                dob,
                emailId: email,
                password: hashedPassword,
            }
    
            const registeredUserData = await User.create(user)
    
            return response.status(200).json({
                message: "Account created"
            })
        }
        else{
            return response.status(400).json(validationResult.error.message)
        }
    }
    catch(error){
        next(error)
    }
}

//Authenticate user login
const login = async(request,response,next)=>{
    try{
        const{ email, password } = request.body

        const validationResult = await loginAuthSchema.validate(request.body)

        if(validationResult.error == null){

            const userData = await User.findAll({
                where: {
                    emailId: email
                }
            })
     
            if(userData.length == 0 || !await bcrypt.compare(password, userData[0].password)){      //userData array contains all the information about user at 0 index
                return response.status(401).json({
                    message: "Email or password is incorrect"
                })
            }
            else{
                jwt.sign({ user: userData[0] }, config.secret_key, (error, token) => {          //userData array contains all the information about user at 0 index
                    if(error){
                        return response.status(500).json({
                            message: "Error generating token"
                        })
                    }
                    else{
                        const { password, ...userDetails } = userData[0].dataValues

                        return response.status(200).json({
                            token,
                            user: userDetails                                    
                        })
                    }  
                })  
            }
        }
        else{
            return response.status(400).json(validationResult.error.message)
        }
    }
    catch(error){
        next(error)
    }
}

//All users
const users = async(request,response,next)=>{
    try{
        const userData = await User.findAll()
        
        return response.status(200).json(
            userData
        )
    }
    catch(error){
        next(error)
    }
}

module.exports = {
    signup,
    login,
    users,
}