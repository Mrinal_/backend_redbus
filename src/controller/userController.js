const User = require("../models/User")
const { updateUserAuthSchema } = require("../schemas/authSchemas")

const updateUser = async(request, response, next) => {
    try{

        const { userId, firstName, lastName, phoneNumber} = request.body

        const validationResult = await updateUserAuthSchema.validate(request.body)

        if(validationResult.error == null){

            let user = {}

            if(firstName !== ""){
                user["firstName"] = firstName
            }

            if(lastName !== ""){
                user["lastName"] = lastName
            }

            if(phoneNumber !== ""){
                user["phoneNumber"] = phoneNumber
            }

            const updatedUser = await User.update(
                user,{
                    where: {
                        id: userId
                    }
                }
            )

            if(updatedUser == 1){
                return response.status(200).json({
                    message: "User data updated"
                })
            }
            else{
                return response.status(400).json({
                    message: `Can not update user data. User id ${userId} not found.`
                })
            }
        }
        else{
            return response.status(400).json(validationResult.error.message)
        } 
    }
    catch(error){
        next(error)
    }
}

module.exports = {
    updateUser,
}