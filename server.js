const config = require("./src/config/config")
const { connection, umzug } = require("./src/models/Connection")
const User = require("./src/models/User")
const Amenities = require("./src/models/Amenities")
const Bus_Amenities = require("./src/models/Bus_Amenities")
const Bus = require("./src/models/Bus")
const BusStop = require("./src/models/BusStop")
const City = require("./src/models/City")
const Operator = require("./src/models/Operator")
const Passenger = require("./src/models/Passenger")
const Ticket = require("./src/models/Ticket")
const Trip = require("./src/models/Trip")
const TripStops = require("./src/models/TripStops")
const Route = require("./src/models/Route")
const Seats = require("./src/models/Seats")
const express = require("express")
const bodyParser = require("body-parser")
const authRoutes = require("./src/routes/authRoutes")
const userRoutes = require("./src/routes/userRoutes")
const searchRoutes = require("./src/routes/searchRoutes")
const bookingRoutes = require("./src/routes/bookingRoutes")
const jwt = require("jsonwebtoken")
const { secret_key } = require("./src/config/config")
const cors = require("cors")

//Init express
const app = express()

app.use(cors())

//Body parser

app.use(bodyParser.urlencoded({ 
    extended: true, 
    limit: "50mb" 
}))

app.use(bodyParser.json({ 
    limit: "50mb" 
}))

//API routes
app.use("/auth", authRoutes)

app.use("/user", verifyToken, userRoutes)

app.use("/search", searchRoutes)

app.use("/booking", bookingRoutes)

//Middleware to handle errors 
app.use((request, response, next)=>{
    response.status(404).json({
        message: "404 page not found"
    }).end()
})    

app.use((error, request, response, next)=>{
    console.log(error)
    response.status(500).json({
        message: "Can not serve data right now. Please try again later."
    }).end()
})

function verifyToken (request, response, next) {
    const bearerHeader = request.headers['authorization']
    if (typeof bearerHeader !== "undefined") {
        const token = bearerHeader.split(" ")[1]
        request.token = token

        jwt.verify(request.token, secret_key, (error, user) =>{
            if(error){
                return response.status(403).json({
                    message: "Please log in again"
                })
            }
            else{
                next()
            }
        })
    }
    else {
        response.sendStatus(403);               
    }
}

//Associations

Bus.hasMany(Route, { foreignKey: "bus_id" })
Route.belongsTo(Bus , { foreignKey: "bus_id" })
Operator.hasMany(Bus, { foreignKey: "operator_id" })
Bus.belongsTo(Operator, { foreignKey: "operator_id" }) 
Bus.belongsToMany(Amenities, { through: "bus_amenities", foreignKey: "bus_id", otherKey: "amenities_id"})
Route.hasMany(Seats, { foreignKey: "route_id" })
Seats.belongsTo(Route , { foreignKey: "route_id" })
City.hasMany(BusStop, { foreignKey: "city_id" })
BusStop.belongsTo(City , { foreignKey: "city_id" })
Bus.hasMany(Trip, { foreignKey: "bus_id" })
Trip.belongsTo(Bus, { foreignKey: "bus_id" })
Trip.belongsTo(City, { foreignKey: "sourceCity" , as: "sourceCityName"})
Trip.belongsTo(City, { foreignKey: "destinationCity", as: "destinationCityName" })
Trip.hasMany(Passenger, { foreignKey: "trip_id" })
Passenger.belongsTo(Trip, { foreignKey: "trip_id" })
Trip.hasOne(Ticket, { foreignKey: "trip_id" })
Ticket.belongsTo(Trip, { foreignKey: "trip_id" })
Route.hasMany(Trip, { foreignKey: "route_id" })
Trip.belongsTo(Route, { foreignKey: "route_id" })

umzug.up()
    .then(()=>{
        app.listen(config.port, ()=>{
            console.log(`Server running at port ${config.port}`)
        })
    })
    .catch((error)=>{
        console.log(error)
    })
